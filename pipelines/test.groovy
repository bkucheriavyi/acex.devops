def appRepo = 'git@bitbucket.org:ormlondon/acex.journeysearch.git'
def bitbucketCredsId = 'bitbutket-key'
def ecrRepo = '878033925423.dkr.ecr.eu-west-3.amazonaws.com'
def ecrCredsId = 'ecr:eu-west-3:acex_test'

podTemplate(label: 'docker',
        containers: [containerTemplate(name: 'docker', image: 'docker:1.11', ttyEnabled: true, command: 'cat')],
        volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')]
) {

    def image = "jenkins/jnlp-slave"
    node('docker') {
        checkout([$class: 'GitSCM',
                  branches: [[name: 'migrate-to-new-nuget-feed']],
                  doGenerateSubmoduleConfigurations: false,
                  extensions: [
                          [$class: 'CleanCheckout']
                  ],
                  submoduleCfg: [],
                  userRemoteConfigs: [[credentialsId: bitbucketCredsId, url: appRepo]]
        ])
        properties([
                parameters([
                        string(name: 'GitUrl', defaultValue: 'None', description: '', ),
                        string(name: 'GitBranch', defaultValue: 'None', description: '', ),
                        string(name: 'GitCommit', defaultValue: 'None', description: '', )
                ])
        ])
        stage('Build Docker image') {
           // git 'https://github.com/jenkinsci/docker-jnlp-slave.git'
            container('docker') {
                sh "pwd"
                sh "ls -la"
                sh "docker run --rm -it alpine ping 8.8.8.8"
               // sh "docker build -t ${image} ."
            }
        }
    }
}