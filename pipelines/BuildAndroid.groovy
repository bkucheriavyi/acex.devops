// Deploy_Backend_v2
// http://172.30.0.177:8080/job/QA/job/Deploy_Backend_v2/
// Repo URL: https://bitbucket.org/Femitelemedicine/devops.git
// Script Path: jenkins_pipelines/Deploy_Backend_v2.groovy

import org.yaml.snakeyaml.Yaml

@NonCPS
def yamlParse(def yamlText) {
  new org.yaml.snakeyaml.Yaml().load(yamlText)
}

//parameters {
//  choices(name: 'namespace',
//          choices: 'dev\nqa',
//          description: 'What door do you choose?')
//  text(defaultValue: '''journeysearch:
//          image: latest
//          service2:
//                  image: latest
//          service3:
//                  image: latest''',
//          name: 'services' )
//}

// currentBuild.description = "Deployment: ${namespace}"

node("android")
        {

          def appRepo = '${GitUrl}'
          def branchRepo = '${GitBranch}'
          def bitbucketCredsId = 'bitbutket-key'
          def ecrRepo = '878033925423.dkr.ecr.eu-west-3.amazonaws.com'
          def ecrCredsId = 'ecr:eu-west-3:acex_test'
          def app_center_token = '6a9daece225aa337226391dd5fe53fd6d3f7786a'
          def app_center_app = 'ORM-AUKB/AUKB-Android'
          def app_center_distributiongroup = 'AndroidTest'

          cleanWs()
          try {


            def workspace = pwd()
            stage('Git checkout') {
              checkout([$class: 'GitSCM',
                        branches: [[name: '${GitBranch}']],
                        doGenerateSubmoduleConfigurations: false,
                        extensions: [
                                [$class: 'CleanCheckout']
                        ],
                        submoduleCfg: [],
                        userRemoteConfigs: [[credentialsId: bitbucketCredsId, url: appRepo]]
              ])
              shortCommit = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
              repoName = sh(script: "git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'", returnStdout: true).trim()

              stage('test') {
                  container('android') {
                      println 'Code has been cloned current brach is: '
                    //  sh("git checkout ${GitBranch}")
                    //  sh("./gradlew sonarqube -Dsonar.host.url=https://sonar.develop.acex.tech/ -Dsonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953'")
                  }
              }
            }

            stage('Build') {
              configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
              }
              configFileProvider([configFile(fileId: 'keystore_properties', variable: 'keystore_properties')]) {
                sh("cp ${keystore_properties} /home/jenkins/workspace/CICD/Android/keystore_properties")
              }
              configFileProvider([configFile(fileId: 'keystore_file', variable: 'keystore_file')]) {
                sh("cp ${keystore_file} /home/jenkins/workspace/CICD/Android/keystore_file")
              }
              configFileProvider([configFile(fileId: 'bit_ssh', variable: 'bit_ssh')]) {
                sh("cp ${bit_ssh} ~/.ssh/id_rsa")
              }
              configFileProvider([configFile(fileId: 'sshagent_config', variable: 'sshagent_config')]) {
                sh("cp ${sshagent_config} ~/.ssh/config")
              }


              container('android') {
                sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit} and ShortCommit ${shortCommit}")
                sh("pwd")
                sh("chmod 600 ~/.ssh/id_rsa")
                sh("cat ~/.ssh/config >> /etc/ssh/ssh_config")
                sh("mkdir -p /root/.ssh/ && touch /root/.ssh/known_hosts")
                sh("cp -rp ~/.ssh/id_rsa /root/.ssh/")
                sh("ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts")
                sh("cat ./keystore_properties > keystore.properties")
                sh("cat ./keystore_file | base64 --decode > debug-keystore.jks")
                sh("git checkout develop")
                println 'EXTERNAL TESTING PART'
                //  sh("tail -f /dev/null")
                // sh("./scripts/ci/build.sh --build-target StagingDebug --branch ${GitBranch}")
               // println 'DEBUG PART'
               // sh("./scripts/ci/build.sh --build-target debug --branch ${GitBranch}")
                 git_commit_message= sh(script: "git log -1 --pretty=%B", returnStdout: true).trim()
                  git_branch=sh(script: "git branch", returnStdout: true).trim()
                 println ("Commit Message = ${git_commit_message}")
                 println ("Git Branch = ${git_branch}")
                  sh("./scripts/ci/build.sh --build-target StagingDebug --branch develop")
                //  sh("tail -f /dev/null")
               //   if (git_commit_message.matches("Merged(.*)")) {
               //       println ("Commit Message = ${git_commit_message}")
               //       sh("./scripts/ci/update-version-properties.sh --minor")
               //   } else {
               //       println ("Commit Message = ${git_commit_message}")
               //       sh("./scripts/ci/update-version-properties.sh --patch")
               //   }
               // sh("sed 's/outputs\\/apk\\/\${build_target}\\/app-\${build_target}.apk/.\\/app\\/build\\/outputs\\/apk\\/\${build_target}\\/app-\${build_target}.apkg/g' scripts/ci/publish.sh")

              }

            }


              stage('Deploy') {
                  configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                      sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
                  }
                  container('android') {
                      distribution_id = "E1OETRRUQMVW3L"
                      sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit} and ShortCommit ${shortCommit}")
                      println 'Publish to Devs'
                     // sh("git checkout ${GitBranch}")
                   //   sh("tail -f /dev/null")
                      sh("./scripts/ci/publish_jenkins.sh --build-target StagingDebug --app-center-token ${app_center_token} --app-center-app ${app_center_app} --app-center-distributiongroup ${app_center_distributiongroup}")
                   //   sh("./scripts/ci/publish_jenkins.sh --build-target externalTesting --app-center-token ${app_center_token} --app-center-app ${app_center_app} --app-center-distributiongroup ${app_center_distributiongroup}")

                    //  sh("./scripts/ci/publish_jenkins.sh --build-target debug --app-center-token ${app_center_token} --app-center-app ${app_center_app} --app-center-distributiongroup ${app_center_distributiongroup}")
                  }
              }





//    sendReportToSlack(namespace, params.services)
            currentBuild.result = "SUCCESS"

          } catch(e) {
//    sendReportToSlack(namespace, params.services, e)
            currentBuild.result = "FAILED"
            println "error: " + e
            throw e
          }
        }