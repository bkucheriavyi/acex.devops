FROM microsoft/dotnet:2.2-sdk AS build
RUN chmod +x /usr/bin/dotnet
RUN chown -R 1001:0 $HOME && \
    chmod -R g+rw $HOME

USER 1001
