variable "aws_region" {}
variable "aws_profile" {}
data "aws_availability_zones" "available" {}
variable "allowed_cidr_ranges" {
  description = "[] List of ipaddresses to be whitelisted in various securitygroups"
  type = "list"
}

variable "ENV_TAG" {}
variable "vpc_name" {}
variable "eks_iam_users" {
  type = "list"
  description = "Additional IAM users to add to the aws-auth configmap."
  default = []
}
variable "eks_iam_users_count" {
  description = "The count of roles in the map_users list."
  type        = "string"
  default     = 0
}

variable "route53_role_policy_document" {
  description = "Role policy document for EKS addon external-dns"
  type        = "string"
  default     = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "route53:ChangeResourceRecordSets"
            ],
            "Resource": [
                "arn:aws:route53:::hostedzone/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:ListHostedZones",
                "route53:ListResourceRecordSets"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}
#------- vpc
variable "vpc_cidr" {}

#------- subnets
variable "cidrs" {
  type = "map"
}

variable "accessip" {}
variable "domain_name" {}
variable "db_instance_class" {}
variable "dbname" {}
variable "dbuser" {}
variable "dbpassword" {}

variable "elb_healthy_threshold" {}
variable "elb_interval" {}
variable "elb_timeout" {}
variable "elb_unhealthy_threashold" {}

variable "public_key_path" {}
variable "key_name" {}
variable "dev_ami" {}
variable "dev_instance_type" {}

variable "lc_instance_type" {}

variable "asg_max" {}
variable "asg_min" {}
variable "asg_grace" {}
variable "asg_hct" {}
variable "asg_cap" {}

variable "delegation_set" {}
